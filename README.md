# Docker 应用商店

本项目由 [`新疆萌森软件开发工作室`](https://blog.lifebus.top/) 提供技术支持，我们致力于为 `Docker Compose`
应用商店用户提供更多的应用程序。

本仓库中的所有应用均来自官方推荐应用，我们会定期更新应用，如果您有任何问题，请联系我们。

---
<div style="border: 1px solid #e1e1e1; border-radius: 8px; box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1); padding: 16px; margin: 16px; background-color: #fff; width: 80%; margin-left: auto; margin-right: auto;">
  <h2 style="margin: 0 0 8px 0; font-size: 1.5em;">2024年12月04日 公告</h2>
  <p>我们已支持应用商店: <span style="font-weight: bold; color: #d9534f;">1Panel</span>, <span style="font-weight: bold; color: #d9534f;">DPanel</span>, <span style="font-weight: bold; color: #d9534f;">Dockge</span></p>
  <p>即将支持应用商店: <span style="font-weight: bold; color: #d9534f;">CasaOS</span></p>
  <p>近期我们上架了很多新应用，如果您有任何问题，请联系我们。</p>
  <p>欢迎投稿，我们会根据您的需求，上架更多的应用。</p>
  <p>🥰 <a href="https://blog.lifebus.top/" target="_blank" >新疆萌森软件开发工作室</a> 祝您生活愉快！</p>
</div>


<div style="border: 1px solid #e1e1e1; border-radius: 8px; box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1); padding: 16px; margin: 16px; background-color: #fff; width: 80%; margin-left: auto; margin-right: auto;">
  <h2 style="margin: 0 0 8px 0; font-size: 1.5em;">2024年11月28日 公告</h2>
  <p>我们已支持应用商店: <span style="font-weight: bold; color: #d9534f;">1Panel</span>, <span style="font-weight: bold; color: #d9534f;">Dockge</span></p>
  <p>即将支持应用商店: <span style="font-weight: bold; color: #d9534f;">DPanel</span></p>
  <p>近期我们上架了很多新应用，如果您有任何问题，请联系我们。</p>
  <p>欢迎投稿，我们会根据您的需求，上架更多的应用。</p>
  <p>🥰 <a href="https://blog.lifebus.top/" target="_blank" >新疆萌森软件开发工作室</a> 祝您生活愉快！</p>
</div>

---

## 应用维护状态

| 状态 |                                                图标                                                |          应用名称           |                             应用官网                              | 短的描述                                                                 |    集合    |
|:--:|:------------------------------------------------------------------------------------------------:|:-----------------------:|:-------------------------------------------------------------:|:---------------------------------------------------------------------|:--------:|
| 🟢 |       <img height="22" src="https://file.lifebus.top/apps/acg-faka/logo.png" width="22"/>        |        异次元店铺系统V3        |                      https://faka.wiki/                       | 二次元发卡系统                                                              |          |
| 🟢 |       <img height="22" src="https://file.lifebus.top/apps/mcy-shop/logo.png" width="22"/>        |        异次元店铺系统V4        |                     https://wiki.mcy.im/                      | 二次元发卡系统                                                              |          |
| 🟢 |        <img height="22" src="https://file.lifebus.top/apps/affine/logo.png" width="22"/>         |         AFFiNE          |                      https://affine.pro/                      | AFFiNE是一个具有完全合并文档、白板和数据库的工作空间                                        |          |
| 🟢 |         <img height="22" src="https://file.lifebus.top/apps/alist/logo.png" width="22"/>         |          AList          |                     https://alist.nn.ci/                      | 一款支持多重存储的文件列表程序                                                      |          |
| 🟢 |         <img height="22" src="https://file.lifebus.top/apps/alist/logo.png" width="22"/>         |       AList-Aria2       |                     https://alist.nn.ci/                      | 【含Aria2插件版】一款支持多重存储的文件列表程序                                           |          |
| 🟢 |         <img height="22" src="https://file.lifebus.top/apps/alist/logo.png" width="22"/>         |      AList-FFmpeg       |                     https://alist.nn.ci/                      | 【含FFmpeg插件版】一款支持多重存储的文件列表程序                                          |          |
| 🟢 |         <img height="22" src="https://file.lifebus.top/apps/ammds/logo.png" width="22"/>         |          AMMDS          |                  https://ammds.lifebus.top/                   | 个人影视数据管理平台                                                           |          |
| 🟢 |        <img height="22" src="https://file.lifebus.top/apps/ani-rss/logo.png" width="22"/>        |         ANI-RSS         |                   https://docs.wushuo.top/                    | 自动追番、订阅、下载 蜜柑RSS动漫                                                   |          |
| 🟢 |     <img height="22" src="https://file.lifebus.top/apps/apifox-runner/logo.png" width="22"/>     |      Apifox Runner      |                      https://apifox.com/                      | 自动化测试定时任务、接口导入、Mock 服务                                               |          |
| 🟢 |    <img height="22" src="https://file.lifebus.top/apps/aria2-superng6/logo.png" width="22"/>     |     Aria2 Superng6版     |           https://github.com/SuperNG6/docker-aria2/           | Docker Aria2的最佳实践                                                    |          |
| 🟢 |        <img height="22" src="https://file.lifebus.top/apps/artalk/logo.png" width="22"/>         |         Artalk          |                    https://artalk.js.org/                     | 一个自托管的评论系统                                                           |          |
| 🟢 |       <img height="22" src="https://file.lifebus.top/apps/auto-lady/logo.png" width="22"/>       |   AutoLady (ByteMuse)   |          https://github.com/envyafish/AutoLadyIssue/          | 自动化订阅AV影片                                                            |          |
| 🟢 |         <img height="22" src="https://file.lifebus.top/apps/baota/logo.png" width="22"/>         |           宝塔            |                      https://www.bt.cn/                       | 宝塔,安全高效的服务器运维面板                                                      |          |
| 🟢 |         <img height="22" src="https://file.lifebus.top/apps/bark/logo.png" width="22"/>          |          Bark           |                     https://bark.day.app/                     | 一款注重隐私、安全可控的自定义通知推送工具                                                |          |
| 🟢 |        <img height="22" src="https://file.lifebus.top/apps/beszel/logo.png" width="22"/>         |       Beszel Hub        |                      https://beszel.dev/                      | 【中心端 - Hub】轻量级的服务器监控平台，包含 Docker 统计信息、历史数据和警报功能                      |  Beszel  |
| 🟢 |        <img height="22" src="https://file.lifebus.top/apps/beszel/logo.png" width="22"/>         |      Beszel Agent       |                      https://beszel.dev/                      | 【监控端 - Agent】轻量级的服务器监控平台，包含 Docker 统计信息、历史数据和警报功能                    |  Beszel  |
| 🟢 |       <img height="22" src="https://file.lifebus.top/apps/bitmagnet/logo.png" width="22"/>       |        Bitmagnet        |                     https://bitmagnet.io/                     | 自托管的 BitTorrent 索引器                                                  |          |
| 🟢 |       <img height="22" src="https://file.lifebus.top/apps/bookstack/logo.png" width="22"/>       |        BookStack        |                 https://www.bookstackapp.com/                 | 一个用于存储和组织信息及文档的平台                                                    |          |
| 🟢 |        <img height="22" src="https://file.lifebus.top/apps/casdoor/logo.png" width="22"/>        |         Casdoor         |                     https://casdoor.org/                      | 身份和访问管理(IAM)/单点登录(SSO)平台                                             |          |
| 🟢 |       <img height="22" src="https://file.lifebus.top/apps/certimate/logo.png" width="22"/>       |        Certimate        |                  https://docs.certimate.me/                   | SSL证书管理工具                                                            |          |
| 🟢 |     <img height="22" src="https://file.lifebus.top/apps/cookie-cloud/logo.png" width="22"/>      |      Cookie Cloud       |           https://github.com/easychen/CookieCloud/            | CookieCloud是一个和自架服务器同步浏览器Cookie和LocalStorage的小工具                     |          |
| 🟢 |       <img height="22" src="https://file.lifebus.top/apps/cyberchef/logo.png" width="22"/>       |        CyberChef        |              https://github.com/gchq/CyberChef/               | 一个用于加密、编码、压缩和数据分析的网页应用程序                                             |          |
| 🟢 |        <img height="22" src="https://file.lifebus.top/apps/dashdot/logo.png" width="22"/>        |          Dash.          |                    https://getdashdot.com/                    | 现代服务器仪表板                                                             |          |
| 🟢 |        <img height="22" src="https://file.lifebus.top/apps/dashdot/logo.png" width="22"/>        |       Dash.(GPU)        |                    https://getdashdot.com/                    | 【GPU支持】现代服务器仪表板                                                      |          |
| 🟢 |        <img height="22" src="https://file.lifebus.top/apps/deeplx/logo.png" width="22"/>         |         DeepLX          |                  https://deeplx.owo.network/                  | DeepL免费API（无需TOKEN）                                                  |          |
| 🟢 |         <img height="22" src="https://file.lifebus.top/apps/dify/logo.png" width="22"/>          |          Dify           |                       https://dify.ai/                        | Dify 是一个开源的 LLM 应用开发平台                                               |          |
| 🟢 |        <img height="22" src="https://file.lifebus.top/apps/dockge/logo.png" width="22"/>         |         Dockge          |                   https://dockge.kuma.pet/                    | 面向堆栈的管理器                                                             |          |
| 🟢 |        <img height="22" src="https://file.lifebus.top/apps/dozzle/logo.png" width="22"/>         |         Dozzle          |                      https://dozzle.dev/                      | 一个轻量级的小应用程序，有一个基于web的界面来监控Docker日志                                   |          |
| 🟢 |        <img height="22" src="https://file.lifebus.top/apps/dpanel/logo.png" width="22"/>         |         DPanel          |                      https://dpanel.cc/                       | Docker可视化管理面板                                                        |          |
| 🟢 |       <img height="22" src="https://file.lifebus.top/apps/dujiaoka/logo.png" width="22"/>        |          独角数卡           |             https://github.com/assimon/dujiaoka/              | 独角数卡(自动售货系统)-开源站长自动化售货解决方案、高效、稳定、快速                                  |          |
| 🟢 |      <img height="22" src="https://file.lifebus.top/apps/eigenfocus/logo.png" width="22"/>       |       Eigenfocus        |                    https://eigenfocus.com/                    | 看板、时间跟踪与专注工具                                                         |          |
| 🟢 | <img height="22" src="https://file.lifebus.top/apps/elastic-elasticsearch/logo.png" width="22"/> |      Elasticsearch      |             https://www.elastic.co/elasticsearch/             | Elasticsearch 是一个分布式、RESTful 风格的搜索和数据分析引擎                            | Elastic  |
| 🟢 |    <img height="22" src="https://file.lifebus.top/apps/elastic-kibana/logo.png" width="22"/>     |         Kibana          |                https://www.elastic.co/kibana/                 | Kibana 针对大规模数据快速运行数据分析                                               | Elastic  |
| 🟢 |         <img height="22" src="https://file.lifebus.top/apps/emby/logo.png" width="22"/>          |          Emby           |                      https://emby.media/                      | Emby Server 是一款个人媒体服务器，可在几乎所有设备上运行应用程序                               |   Emby   |
| 🟢 |         <img height="22" src="https://file.lifebus.top/apps/emby/logo.png" width="22"/>          |       Emby-amilys       |                      https://emby.media/                      | 【开心版 By amilys】Emby Server 是一款个人媒体服务器，可在几乎所有设备上运行应用程序                |   Emby   |
| 🟢 |         <img height="22" src="https://file.lifebus.top/apps/emby/logo.png" width="22"/>          |      Emby-lovechen      |                      https://emby.media/                      | 【开心版 By lovechen】Emby Server 是一款个人媒体服务器，可在几乎所有设备上运行应用程序              |   Emby   |
| 🟢 |       <img height="22" src="https://file.lifebus.top/apps/feedback/logo.png" width="22"/>        |        feedback         |       https://gitcode.com/q23163677431/feedback-docker/       | 问题反馈平台 (替兔小巢)                                                        |          |
| 🟢 |         <img height="22" src="https://file.lifebus.top/apps/fider/logo.png" width="22"/>         |          Fider          |                       https://fider.io/                       | 简单而优雅的功能投票工具                                                         |          |
| 🟢 |     <img height="22" src="https://file.lifebus.top/apps/flaresolverr/logo.png" width="22"/>      |      FlareSolverr       |         https://github.com/FlareSolverr/FlareSolverr/         | 用于绕过Cloudflare保护的代理服务器                                               |          |
| 🟢 |         <img height="22" src="https://file.lifebus.top/apps/ghost/logo.png" width="22"/>         |          Ghost          |                      https://ghost.org/                       | 用于现代出版、会员制、订阅和通讯的人博客系统                                               |          |
| 🟢 |         <img height="22" src="https://file.lifebus.top/apps/gitea/logo.png" width="22"/>         |          Gitea          |                       https://gitea.io/                       | 私有、快速、可靠的 DevOps 平台                                                  |  Gitea   |
| 🟢 |         <img height="22" src="https://file.lifebus.top/apps/gitea/logo.png" width="22"/>         |      Gitea Runner       |                       https://gitea.io/                       | 【Runner】私有、快速、可靠的 DevOps 平台                                          |  Gitea   |
| 🟢 |        <img height="22" src="https://file.lifebus.top/apps/glance/logo.png" width="22"/>         |         Glance          |             https://github.com/glanceapp/glance/              | 一个自托管的仪表板，将所有您的订阅源放在一个地方                                             |          |
| 🟢 |         <img height="22" src="https://file.lifebus.top/apps/halo/logo.png" width="22"/>          |          Halo           |                       https://halo.run/                       | 强大易用的开源建站工具                                                          |          |
| 🟢 |        <img height="22" src="https://file.lifebus.top/apps/homarr/logo.png" width="22"/>         |         Homarr          |                      https://homarr.dev/                      | 一个时尚、现代化的仪表板                                                         |          |
| 🟢 |        <img height="22" src="https://file.lifebus.top/apps/ikaros/logo.png" width="22"/>         |         ikaros          |              https://github.com/Suwmlee/ikaros/               | 打通下载软件与媒体服务，安心享受影片                                                   |          |
| 🟢 |       <img height="22" src="https://file.lifebus.top/apps/iframely/logo.png" width="22"/>        |        Iframely         |                     https://iframely.com/                     | 响应式 Web 嵌入和 URL 元的 Iframely API                                      |          |
| 🟢 |        <img height="22" src="https://file.lifebus.top/apps/immich/logo.png" width="22"/>         |         Immich          |                      https://immich.app/                      | 【完整版本】高性能自托管照片和视频管理解决方案                                              |  Immich  |
| 🟢 |        <img height="22" src="https://file.lifebus.top/apps/immich/logo.png" width="22"/>         |    Immich-No-Machine    |                      https://immich.app/                      | 【无机器学习】高性能自托管照片和视频管理解决方案，适用于性能较差机器使用                                 |  Immich  |
| 🟢 |        <img height="22" src="https://file.lifebus.top/apps/immich/logo.png" width="22"/>         |      Immich-Server      |                      https://immich.app/                      | 【主服务模块】高性能自托管照片和视频管理解决方案，适用于远端数据库，远端机器学习                             |  Immich  |
| 🟢 |        <img height="22" src="https://file.lifebus.top/apps/immich/logo.png" width="22"/>         | Immich-Machine-Learning |                      https://immich.app/                      | 【机器学习模块】高性能自托管照片和视频管理解决方案，适用于作为远端机器学习使用                              |  Immich  |
| 🟢 |       <img height="22" src="https://file.lifebus.top/apps/iyuu-plus/logo.png" width="22"/>       |        IYUU Plus        |                     https://doc.iyuu.cn/                      | 基于特征码的索引工具                                                           |          |
| 🟢 |         <img height="22" src="https://file.lifebus.top/apps/iycms/logo.png" width="22"/>         |          爱影CMS          |                    https://www.iycms.com/                     | 高性能高并发永久免费影视CMS                                                      |          |
| 🟢 |       <img height="22" src="https://file.lifebus.top/apps/jellyfin/logo.png" width="22"/>        |        Jellyfin         |                     https://jellyfin.org/                     | 自由软件媒体系统                                                             |          |
| 🟢 |      <img height="22" src="https://file.lifebus.top/apps/jellyseerr/logo.png" width="22"/>       |       Jellyseerr        |                 https://docs.jellyseerr.dev/                  | 满足您所有媒体需求的一站式平台                                                      |          |
| 🟢 |        <img height="22" src="https://file.lifebus.top/apps/jproxy/logo.png" width="22"/>         |         JProxy          |           https://github.com/LuckyPuppy514/jproxy/            | 介于 Sonarr / Radarr 和 Jackett / Prowlarr 之间的代理，主要用于优化查询和提升识别率         |          |
| 🟢 |        <img height="22" src="https://file.lifebus.top/apps/kavita/logo.png" width="22"/>         |         Kavita          |                 https://www.kavitareader.com/                 | 自托管数字图书馆                                                             |          |
| 🟢 |         <img height="22" src="https://file.lifebus.top/apps/kimai/logo.png" width="22"/>         |          Kimai          |                    https://www.kimai.org/                     | 基于网络的多用户时间追踪应用                                                       |          |
| 🟢 |         <img height="22" src="https://file.lifebus.top/apps/komga/logo.png" width="22"/>         |          Komga          |                      https://komga.org/                       | 漫画、漫画、蓝光光盘、杂志和电子书的媒体服务器                                              |          |
| 🟢 |      <img height="22" src="https://file.lifebus.top/apps/limesurvey/logo.png" width="22"/>       |           问卷星           |                  https://www.limesurvey.org/                  | 在线问卷调查程序                                                             |          |
| 🟢 |       <img height="22" src="https://file.lifebus.top/apps/linkding/logo.png" width="22"/>        |        LinkDing         |           https://github.com/sissbruecker/linkding/           | 自托管书签管理器                                                             |          |
| 🟢 |         <img height="22" src="https://file.lifebus.top/apps/logto/logo.png" width="22"/>         |          Logto          |                       https://logto.io/                       | 开发者更好的身份基础设施，以及 Auth0 的开源替代方案。                                       |          |
| 🟢 |       <img height="22" src="https://file.lifebus.top/apps/maccms10/logo.png" width="22"/>        |        苹果CMS V10        |                    https://www.maccms.la/                     | 基于ThinkPHP和Layui的多功能开源免费内容管理系统                                       |          |
| 🟢 |      <img height="22" src="https://file.lifebus.top/apps/mailserver/logo.png" width="22"/>       |    Docker Mailserver    | https://docker-mailserver.github.io/docker-mailserver/latest/ | 可用于生产的全栈但简单的邮件服务器                                                    |          |
| 🟢 |        <img height="22" src="https://file.lifebus.top/apps/mdc-ng/logo.png" width="22"/>         |         MDC-NG          |               https://github.com/mdc-ng/mdc-ng/               | 成人电影数据采集工具                                                           |          |
| 🟢 |       <img height="22" src="https://file.lifebus.top/apps/mastodon/logo.png" width="22"/>        |     Mastodon (长毛象)      |                   https://joinmastodon.org/                   | 自由开源的去中心化的分布式微博客社交网络                                                 |          |
| 🟢 |         <img height="22" src="https://file.lifebus.top/apps/maxkb/logo.png" width="22"/>         |          MaxKB          |                       https://maxkb.cn/                       | 基于 LLM 大语言模型的知识库问答系统                                                 |          |
| 🟢 |       <img height="22" src="https://file.lifebus.top/apps/mediacms/logo.png" width="22"/>        |        Media CMS        |                     https://mediacms.io/                      | 现代、功能齐全的开源视频和媒体内容管理系统                                                |          |
| 🟢 |    <img height="22" src="https://file.lifebus.top/apps/metatube-server/logo.png" width="22"/>    |        MetaTube         |            https://github.com/metatube-community/             | 为 Jellyfin/Emby/Plex 开发的超级好用的成人元数据刮削插件                               |          |
| 🟢 |         <img height="22" src="https://file.lifebus.top/apps/minio/logo.png" width="22"/>         |          MinIO          |                        https://min.io/                        | MinIO 是一种高性能、兼容 S3 的对象存储                                             |          |
| 🟢 |      <img height="22" src="https://file.lifebus.top/apps/movie-pilot/logo.png" width="22"/>      |       MoviePilot        |             https://github.com/jxxghp/MoviePilot/             | NAS媒体库自动化管理工具                                                        |          |
| 🟢 |         <img height="22" src="https://file.lifebus.top/apps/mysql/logo.png" width="22"/>         |          MySQL          |                    https://www.mysql.com/                     | 关系数据库管理系统                                                            |  MySQL   |
| 🟢 |        <img height="22" src="https://file.lifebus.top/apps/mariadb/logo.png" width="22"/>        |         MariaDB         |                     https://mariadb.org/                      | 【MySQL分支】创新的开源数据库                                                    |  MySQL   |
| 🟢 |        <img height="22" src="https://file.lifebus.top/apps/percona/logo.png" width="22"/>        |         Percona         |                   https://www.percona.com/                    | 【MySQL分支】关系数据库管理系统                                                   |  MySQL   |
| 🟢 |         <img height="22" src="https://file.lifebus.top/apps/nacos/logo.png" width="22"/>         |          Nacos          |                       https://nacos.io/                       | 动态服务发现、配置管理和服务管理平台                                                   |          |
| 🟢 |        <img height="22" src="https://file.lifebus.top/apps/nexus3/logo.png" width="22"/>         |         Nexus3          |                     https://sonatype.com/                     | Sonatype Nexus存储库                                                    |          |
| 🟢 |       <img height="22" src="https://file.lifebus.top/apps/nexusphp/logo.png" width="22"/>        |        NexusPHP         |                     https://nexusphp.org/                     | 完整的 PT 建站解决方案                                                        |          |
| 🟢 |         <img height="22" src="https://file.lifebus.top/apps/nezha/logo.png" width="22"/>         |          哪吒监控           |                      https://nezha.wiki/                      | 【哪吒监控】开源、轻量、易用的服务器监控、运维工具                                            |          |
| 🟢 |      <img height="22" src="https://file.lifebus.top/apps/nezha-dash/logo.png" width="22"/>       |        NezhaDash        |                https://nezha-cf.buycoffee.top/                | 基于 Next.js 和 哪吒监控 的仪表盘                                               |          |
| 🟢 |       <img height="22" src="https://file.lifebus.top/apps/nginx-ui/logo.png" width="22"/>        |        Nginx UI         |                     https://nginxui.com/                      | 全新的 Nginx 网络管理界面，旨在简化 Nginx 服务器的管理和配置                                |          |
| 🟢 |        <img height="22" src="https://file.lifebus.top/apps/nocodb/logo.png" width="22"/>         |         NocoDB          |                      https://nocodb.com/                      | 无代码数据库平台                                                             |          |
| 🟢 |        <img height="22" src="https://file.lifebus.top/apps/nodebb/logo.png" width="22"/>         |         NodeBB          |                      https://nodebb.org/                      | 一个更适合现代网络的社会平台                                                       |          |
| 🟢 |        <img height="22" src="https://file.lifebus.top/apps/ollama/logo.png" width="22"/>         |         Ollama          |                      https://ollama.com/                      | 一站式大模型部署平台                                                           |          |
| 🟢 |        <img height="22" src="https://file.lifebus.top/apps/onedev/logo.png" width="22"/>         |         OneDev          |                      https://onedev.io/                       | DevOps 平台、带有 CI/CD、看板和软件包的 Git 服务器                                   |          |
| 🟢 |      <img height="22" src="https://file.lifebus.top/apps/open-webui/logo.png" width="22"/>       |       Open Webui        |                    https://openwebui.com/                     | 用户友好的 AI 界面                                                          |          |
| 🟢 |        <img height="22" src="https://file.lifebus.top/apps/outline/logo.png" width="22"/>        |         OutLine         |                  https://www.getoutline.com/                  | 快速、协作的团队知识库                                                          |          |
| 🟢 |       <img height="22" src="https://file.lifebus.top/apps/overseerr/logo.png" width="22"/>       |        overseerr        |                  https://docs.overseerr.dev/                  | 管理您的媒体库请求                                                            |          |
| 🟢 |     <img height="22" src="https://file.lifebus.top/apps/page-spy-web/logo.png" width="22"/>      |         PageSpy         |                   https://www.pagespy.org/                    | 开源调试平台                                                               |          |
| 🟢 |       <img height="22" src="https://file.lifebus.top/apps/paopao-ce/logo.png" width="22"/>       |       泡泡(PaoPao)        |                   https://www.paopao.info/                    | 清新文艺微社区                                                              |          |
| 🟢 |      <img height="22" src="https://file.lifebus.top/apps/photoprism/logo.png" width="22"/>       |       PhotoPrism        |                  https://www.photoprism.app/                  | 面向去中心化网络的人工智能照片应用程序                                                  |          |
| 🟢 |        <img height="22" src="https://file.lifebus.top/apps/pi-hole/logo.png" width="22"/>        |         Pi-hole         |                     https://pi-hole.net/                      | DNS 沉洞服务器                                                            |          |
| 🟢 |        <img height="22" src="https://file.lifebus.top/apps/pi-hole/logo.png" width="22"/>        |      Pi-hole(DHCP)      |                     https://pi-hole.net/                      | 【DHCP支持】DNS 沉洞服务器                                                    |          |
| 🟢 |        <img height="22" src="https://file.lifebus.top/apps/pixman/logo.png" width="22"/>         |         Pixman          |                      https://pixman.io/                       | IPTV、M3U8 流媒体代理方案                                                    |          |
| 🟢 |         <img height="22" src="https://file.lifebus.top/apps/plex/logo.png" width="22"/>          |          Plex           |                     https://www.plex.tv/                      | 媒体播放器及媒体服务器软件                                                        |          |
| 🟢 |     <img height="22" src="https://file.lifebus.top/apps/pingvin-share/logo.png" width="22"/>     |      Pingvin Share      |         https://github.com/stonith404/pingvin-share/          | 一个集轻盈与美观于一体的自托管文件共享平台                                                |          |
| 🟢 |      <img height="22" src="https://file.lifebus.top/apps/postgresql/logo.png" width="22"/>       |       PostgreSQL        |                  https://www.postgresql.org/                  | 世界上最先进的开源关系数据库                                                       |          |
| 🟢 |       <img height="22" src="https://file.lifebus.top/apps/prowlarr/logo.png" width="22"/>        |        Prowlarr         |                     https://prowlarr.com/                     | 终极索引器管理器                                                             |          |
| 🟢 |         <img height="22" src="https://file.lifebus.top/apps/puff/logo.png" width="22"/>          |          Puff           |                https://github.com/BitAUR/Puff/                | 域名监控程序                                                               |          |
| 🟢 |      <img height="22" src="https://file.lifebus.top/apps/qbittorrent/logo.png" width="22"/>      |       qBittorrent       |                 https://www.qbittorrent.org/                  | qBittorrent 比特流客户端                                                   |          |
| 🟢 |       <img height="22" src="https://file.lifebus.top/apps/qinglong/logo.png" width="22"/>        |           青龙            |                   https://qinglong.online/                    | 【青龙】支持 Python3、JavaScript、Shell、Typescript 的定时任务管理平台                 |          |
| 🟢 |        <img height="22" src="https://file.lifebus.top/apps/radarr/logo.png" width="22"/>         |         radarr          |                     https://radarr.video/                     | 电影组织者/管理员，适用于 Usenet 和 torrent 用户                                    |          |
| 🟢 |        <img height="22" src="https://file.lifebus.top/apps/reader3/logo.png" width="22"/>        |           阅读3           |             https://github.com/hectorqin/reader/              | 【阅读3】阅读3服务器版，不需要手机                                                   |          |
| 🟢 |         <img height="22" src="https://file.lifebus.top/apps/redis/logo.png" width="22"/>         |          Redis          |                       https://redis.io/                       | 从世界上最快的内存数据库创建者那里获取该数据库                                              |          |
| 🟢 |        <img height="22" src="https://file.lifebus.top/apps/rsshub/logo.png" width="22"/>         |         RssHub          |                      https://rsshub.app/                      | 从任何内容生成 RSS/Atom/JSON 订阅源                                            |          |
| 🟢 |       <img height="22" src="https://file.lifebus.top/apps/sentinel/logo.png" width="22"/>        |        Sentinel         |                   https://sentinelguard.io/                   | 面向分布式、多语言异构化服务架构的流量治理组件                                              |          |
| 🟢 |        <img height="22" src="https://file.lifebus.top/apps/shiori/logo.png" width="22"/>         |         Shiori          |              https://github.com/go-shiori/shiori              | 简单书签管理器，旨在作为 Pocket 的简单克隆                                            |          |
| 🟢 |        <img height="22" src="https://file.lifebus.top/apps/siyuan/logo.png" width="22"/>         |          思源笔记           |                   https://b3log.org/siyuan/                   | 【思源笔记】一款隐私优先、自托管、完全开源的个人知识管理软件                                       |          |
| 🟢 |         <img height="22" src="https://file.lifebus.top/apps/slink/logo.png" width="22"/>         |          Slink          |          https://github.com/andrii-kryvoviaz/slink/           | 图片分享平台                                                               |          |
| 🟢 |        <img height="22" src="https://file.lifebus.top/apps/sonarr/logo.png" width="22"/>         |         sonarr          |                      https://sonarr.tv/                       | 智能 PVR 适用于新闻组和 BT 用户。                                                |          |
| 🟢 |       <img height="22" src="https://file.lifebus.top/apps/speedtest/logo.png" width="22"/>       |        SpeedTest        |                  https://www.speedtest.net/                   | 互联网速度测试                                                              |          |
| 🟢 |   <img height="22" src="https://file.lifebus.top/apps/speedtest-tracker/logo.png" width="22"/>   |    SpeedTest-Tracker    |              https://docs.speedtest-tracker.dev/              | Speedtest Tracker 是一款自托管互联网性能跟踪应用程序，可针对 Ookla 的 Speedtest 服务运行速度测试检查 |          |
| 🟢 |         <img height="22" src="https://file.lifebus.top/apps/stash/logo.png" width="22"/>         |          Stash          |                     https://stashapp.cc/                      | 组织和刮削你的色情内容                                                          |          |
| 🟢 |      <img height="22" src="https://file.lifebus.top/apps/stream-rec/logo.png" width="22"/>       |       Stream-Rec        |            https://github.com/hua0512/stream-rec/             | Stream-rec是一个用于各种流媒体服务的自动流媒体录制工具                                     |          |
| 🟢 |      <img height="22" src="https://file.lifebus.top/apps/stream-rec/logo.png" width="22"/>       |     Stream-Rec Web版     |            https://github.com/hua0512/stream-rec/             | 【前端版】Stream-rec是一个用于各种流媒体服务的自动流媒体录制工具                                |          |
| 🟢 |      <img height="22" src="https://file.lifebus.top/apps/stream-rec/logo.png" width="22"/>       |     Stream-Rec 服务版      |            https://github.com/hua0512/stream-rec/             | 【服务版】Stream-rec是一个用于各种流媒体服务的自动流媒体录制工具                                |          |
| 🟢 |       <img height="22" src="https://file.lifebus.top/apps/sun-panel/logo.png" width="22"/>       |        Sun Panel        |                  https://doc.sun-panel.top/                   | 一个服务器、NAS导航面板、Homepage、浏览器首页                                         |          |
| 🟢 |  <img height="22" src="https://file.lifebus.top/apps/super-productivity/logo.png" width="22"/>   |   Super Productivity    |                https://super-productivity.com/                | 一个高级TODO列表应用程序，具有时间框和时间跟踪功能                                          |          |
| 🟢 |      <img height="22" src="https://file.lifebus.top/apps/surveyking/logo.png" width="22"/>       |           卷王            |                    https://surveyking.cn/                     | 卷王问卷考试系统                                                             |          |
| 🟢 |        <img height="22" src="https://file.lifebus.top/apps/synapse/logo.png" width="22"/>        |         Synapse         |                      https://matrix.org/                      | 用于安全、分散通信的开放网络                                                       |  Matrix  |
| 🟢 |        <img height="22" src="https://file.lifebus.top/apps/synapse/logo.png" width="22"/>        |      Synapse Admin      |    https://github.com/Awesome-Technologies/synapse-admin/     | 【管理面板】用于安全、分散通信的开放网络                                                 |  Matrix  |
| 🟢 |      <img height="22" src="https://file.lifebus.top/apps/element-web/logo.png" width="22"/>      |       Element Web       |                      https://element.io/                      | 一个用于Web的光泽Matrix协作客户端                                                |  Matrix  |
| 🟢 |    <img height="22" src="https://file.lifebus.top/apps/tangsengdaodao/logo.png" width="22"/>     |          唐僧叨叨           |                     https://tsdaodao.com/                     | 让企业轻松拥有自己的即时通讯                                                       |          |
| 🟢 |        <img height="22" src="https://file.lifebus.top/apps/teemii/logo.png" width="22"/>         |         Teemii          |                    https://www.teemii.io/                     | 漫画阅读器和管理器                                                            |          |
| 🟢 |   <img height="22" src="https://file.lifebus.top/apps/telegram-bot-api/logo.png" width="22"/>    |    Telegram Bot Api     |                     https://telegram.org/                     | 自建 Telegram Bot API 服务器                                              |          |
| 🟢 |        <img height="22" src="https://file.lifebus.top/apps/tissue/logo.png" width="22"/>         |         Tissue          |              https://github.com/chris-2s/tissue/              | 老师教材刮削工具，提供海报下载、元数据匹配等功能                                             |          |
| 🟢 |       <img height="22" src="https://file.lifebus.top/apps/topiam-ce/logo.png" width="22"/>       |         TOPIAM          |                      https://topiam.cn/                       | IAM/IDaaS 身份管理平台                                                     |          |
| 🟢 |        <img height="22" src="https://file.lifebus.top/apps/traccar/logo.png" width="22"/>        |       Traccar 轨迹        |                   https://www.traccar.org/                    | 现代 GPS 追踪系统                                                          |          |
| 🟢 |     <img height="22" src="https://file.lifebus.top/apps/transmission/logo.png" width="22"/>      |      Transmission       |                  https://transmissionbt.com/                  | 快速、简单、免费的 Bittorrent 客户端                                             |          |
| 🟢 |      <img height="22" src="https://file.lifebus.top/apps/uptime-kuma/logo.png" width="22"/>      |       Uptime Kuma       |                   https://uptime.kuma.pet/                    | 自托管监控工具                                                              |          |
| 🟢 |        <img height="22" src="https://file.lifebus.top/apps/v2raya/logo.png" width="22"/>         |         v2rayA          |                      https://v2raya.org/                      | 一个易用而强大的，跨平台的 V2Ray 客户端                                              |          |
| 🟢 |        <img height="22" src="https://file.lifebus.top/apps/vertex/logo.png" width="22"/>         |         Vertex          |             https://github.com/vertex-app/vertex/             | 追剧刷流一体化综合管理工具                                                        |          |
| 🟢 |         <img height="22" src="https://file.lifebus.top/apps/umami/logo.png" width="22"/>         |          Umami          |                       https://umami.is/                       | 为速度和效率而构建的网站分析                                                       |          |
| 🟢 |     <img height="22" src="https://file.lifebus.top/apps/watchyourlan/logo.png" width="22"/>      |         路由设备监控          |           https://github.com/aceberg/WatchYourLAN/            | 轻量级网络 IP 扫描器                                                         |          |
| 🟢 |        <img height="22" src="https://file.lifebus.top/apps/weblate/logo.png" width="22"/>        |         Weblate         |                     https://weblate.org/                      | 高度集成了版本控制功能的web-based翻译工具                                            |          |
| 🟢 |        <img height="22" src="https://file.lifebus.top/apps/windows/logo.png" width="22"/>        |       Windows 系统        |                  https://www.microsoft.com/                   | Docker 容器内的 Windows                                                  |    OS    |
| 🟢 |         <img height="22" src="https://file.lifebus.top/apps/macos/logo.png" width="22"/>         |         Mac OS          |                    https://www.apple.com/                     | Docker 容器内的 Mac OS                                                   |    OS    |
| 🟢 |       <img height="22" src="https://file.lifebus.top/apps/wiseflow/logo.png" width="22"/>        |        WiseFlow         |           https://github.com/TeamWiseFlow/wiseflow/           | Wiseflow 首席情报官                                                       |          |
| 🟢 |       <img height="22" src="https://file.lifebus.top/apps/wukongim/logo.png" width="22"/>        |          悟空IM           |                     https://githubim.com/                     | 让信息传递更简单                                                             |          |
| 🟢 |     <img height="22" src="https://file.lifebus.top/apps/xunlei-cnk3x/logo.png" width="22"/>      |         迅雷 NAS版         |               https://github.com/cnk3x/xunlei/                | 迅雷远程下载服务 NAS移植版                                                      |          |
| 🔴 |                                                                                                  |          Yarr           |                https://github.com/nkanaev/yarr                | Rss 阅读器                                                              |          |
| 🟡 |       <img height="22" src="https://file.lifebus.top/apps/zerotier/logo.png" width="22"/>        |     Zerotier Planet     |                   https://www.zerotier.com/                   | 【自建Planet】ZeroTier 可让您构建几乎任何类型的现代化安全多点虚拟化网络                          | Zerotier |
| 🟡 |       <img height="22" src="https://file.lifebus.top/apps/zerotier/logo.png" width="22"/>        |         Ztncui          |                   https://www.zerotier.com/                   | ZeroTier 可让您构建几乎任何类型的现代化安全多点虚拟化网络                                    | Zerotier |

> 说明：
>
> 以上排序不分先后，按照字母顺序排列。
>
> 维护状态：🟢 维护中 🔴 未维护 🟡 表示不定期维护

---

## 1Pane 导入应用商店

> 温馨提示：
> 当您已安装其他第三方库时并且存在应用冲突，安装过程中会主动删除冲突的第三方库应用，如果您不同意，请不要执行脚本。
>
> 我们建议您在安装之前备份您的数据，或手动安装。
>
> 如果您不使用 `curl` 命令，可以使用 `wget` 命令代替。
>
> 将 `curl -sSL` 替换为 `wget -qO-` 即可。
>
> 脚本执行成功后，会自动将应用添加到 `1Panel` 应用商店中，在应用商店页执行 `更新应用列表` 即可看到新应用。

### 自动化安装

一键式安装，无需手动操作，自动完成 1Panel应用 上架应用商店。

每三小时自动更新一次应用列表。

```sh
curl -sSL https://install.lifebus.top/auto_install.sh | bash
```

> 卸载自动化脚本 (不会卸载应用)

```sh
curl -sSL https://install.lifebus.top/auto_uninstall.sh | bash
```

如需卸载应用列表，您可以手动删除以下目录：

+ 应用目录：`${1panel应用目录}/resource/apps/local`

+ 应用公共文件目录：`/etc/1panel/envs`

+ 应用数据目录：`${应用持久化目录}`

### 手动安装模式

更新时机由自己把握，手动执行命令。

```sh
curl -sSL https://install.lifebus.top/app_install.sh | bash
```

### 计划任务模式

将内容写入Shell计划任务中，设定定期执行。

```sh
#!/bin/bash

# 此处可以定义代理方案

script_url="https://install.lifebus.top/app_install.sh"

echo "Downloading and executing script from $script_url..."
bash <(curl -sL "$script_url")

echo "Script execution completed."

```

#### 配置脚本网络代理

如果您的网络环境需要代理，请在脚本执行前配置代理。

可将以下脚本插入到 `计划任务模式` 的脚本中。

```sh
proxy_protocols="http"
proxy_server="server address"
proxy_port="server port"
export http_proxy="$proxy_protocols://$proxy_server:$proxy_port"
export https_proxy="$proxy_protocols://$proxy_server:$proxy_port"
```

---

### 安装 `Pre 尝鲜版` 应用 (不推荐)

> 说明：
>
> `Pre 尝鲜版` 应用为测试版本，可能存在未知问题，我们建议您在测试环境中使用。

#### 安装尝鲜版应用

```sh
curl -sSL https://install.lifebus.top/pre_app_install.sh | bash
```

#### 卸载尝鲜版应用

```sh
curl -sSL https://install.lifebus.top/pre_app_uninstall.sh | bash
```

---

### 卸载所有本地应用

通过此脚本，您可以卸载所有本地应用(包含您从其他三方库中导入的应用)，但是不会删除数据。

可能您的库中遗留了一些无用的应用，您可以通过此脚本进行清理。该脚本不影响 `自动化脚本` 的应用更新。

**此脚本必须`root`用户执行**

```sh
curl -sSL https://install.lifebus.top/local_app_uninstall.sh | bash
```

## DPanel 导入应用商店

> 温馨提示：
>
> 目前 `DPanel` 商店中的应用由当前仓库构建，适配性可能存在问题，如果您在使用过程中遇到问题，请查看应用的官方文档或社区，或者联系我们。

### 分发仓库

#### Release By Github

```url
https://github.com/QYG2297248353/appstore-dpanel
```

#### Release By Gitee

```url
https://gitee.com/qyg2297248353/appstore-dpanel
```

#### Release By Gitea

```url
https://gitea.com/qyg2297248353/appstore-dpanel
```

```url
https://git.lifebus.top/Ms-Studio/appstore-dpanel
```

#### Release By GitCode

```url
https://gitcode.com/qyg2297248353/appstore-dpanel
```

### 添加教程

请前往 `系统` >> `应用商店` >> `添加第三方应用商店` >> 输入仓库地址 >> 点击 `添加`。

![添加应用商店](https://file.lifebus.top/imgs/dpanel-appstore-add.png)

添加完成后，您可以在 `Compose` >> `应用商店` 中看到新的应用列表。

## Dockge 导入应用商店

> 温馨提示：
> 导入应用列表可能会出现应用冲突，请注意备份数据。

### 手动安装

下载 `dockge` 目录，将 `dockge` 目录内应用列表放置到 `Dockge` 持久化目录下的 `stacks` 目录中。

打开 `Dockge` 面板，点击 `扫描堆栈`，完成导入应用列表。

---

## 常见问题

+ 安装脚本提示
    + 没有权限
        + 请使用 `root` 用户执行脚本
    + `curl: command not found`
        + 请安装 `curl` 命令
        + `apt install curl -y`
        + `yum install curl -y`
    + 由于安装脚本的网络问题导致安装失败
        + 请检查网络状况
        + 请尝试重新执行安装脚本
        + 请尝试使用 `wget` 命令代替 `curl` 命令
+ 升级失败
    + 检查网络状况与磁盘空间
    + 配置镜像地址
    + 尝试重新升级
    + 进行卸载重装
    + 多次升级依旧失败，请联系我们
+ 安装提示
    + `Error: /xxx/xxx/.env file does not exist`
        + 请手动创建 `.env` 文件，不需要填写任何内容的空文件
        + 重新点击安装
+ 前置检查
    + `前置检查` 是利用 `1Panel` 的应用特性，进行安装前的环境检查，如果您的环境不符合要求，将无法安装应用。
    + 关于 `前置检查` 依旧需要填写 `数据库` 相关连接信息，是因为其提供的能力并不是完全可靠，避免后期出现问题，我们采用持久化的方式存储。
    + 如果您的环境符合要求，但是依旧无法安装，请联系我们。

---

## 温馨提示

安装应用前请查看应用说明，了解应用的使用方法和注意事项。
当前第三方库应用均为开源应用，我们不对应用的安全性和稳定性负责。
如果您在使用过程中遇到问题，请查看应用的官方文档或社区，或者联系我们。

---

## 联系我们

<a href="mailto:qyg2297248353@gmail.com" target="_blank" style="display: block; width: 300px; margin: 16px auto; padding: 16px; border: 1px solid #e1e1e1; border-radius: 8px; box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1); background-color: #fff; text-decoration: none; color: #333;">
  <div style="display: flex; align-items: center;">
    <div style="width: 50px; height: 50px; border-radius: 50%; border: 2px solid #e1e1e1; overflow: hidden; margin-right: 16px; display: flex; align-items: center; justify-content: center;">
      <img src="https://file.lifebus.top/imgs/google_gmail_logo.png" alt="Gmail 邮箱" width="32px" style="width: 32px; object-fit: contain;">
    </div>
    <div style="flex: 1;">
      <div style="font-weight: bold;">Gmail 邮箱</div>
      <div style="color: #555; font-size: 0.9em;">通过邮箱反馈您所遇到的问题</div>
    </div>
  </div>
</a>

<a href="https://blog.lifebus.top/" target="_blank" style="display: block; width: 300px; margin: 16px auto; padding: 16px; border: 1px solid #e1e1e1; border-radius: 8px; box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1); background-color: #fff; text-decoration: none; color: #333;">
  <div style="display: flex; align-items: center;">
    <div style="width: 50px; height: 50px; border-radius: 50%; border: 2px solid #e1e1e1; overflow: hidden; margin-right: 16px; display: flex; align-items: center; justify-content: center;">
      <img src="https://file.lifebus.top/imgs/ms_studio_logo.png" alt="人生足迹 · 博客" width="32px" style="width: 32px; object-fit: contain;">
    </div>
    <div style="flex: 1;">
      <div style="font-weight: bold;">人生足迹 · 博客</div>
      <div style="color: #555; font-size: 0.9em;">技术分享的博客平台</div>
    </div>
  </div>
</a>

<a href="https://github.com/QYG2297248353" target="_blank" style="display: block; width: 300px; margin: 16px auto; padding: 16px; border: 1px solid #e1e1e1; border-radius: 8px; box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1); background-color: #fff; text-decoration: none; color: #333;">
  <div style="display: flex; align-items: center;">
    <div style="width: 50px; height: 50px; border-radius: 50%; border: 2px solid #e1e1e1; overflow: hidden; margin-right: 16px; display: flex; align-items: center; justify-content: center;">
      <img src="https://file.lifebus.top/imgs/github_logo.png" alt="GitHub" width="32px" style="width: 32px; object-fit: contain;">
    </div>
    <div style="flex: 1;">
      <div style="font-weight: bold;">GitHub</div>
      <div style="color: #555; font-size: 0.9em;">全球最大同性交友网站</div>
    </div>
  </div>
</a>

<a href="https://git.lifebus.top/" target="_blank" style="display: block; width: 300px; margin: 16px auto; padding: 16px; border: 1px solid #e1e1e1; border-radius: 8px; box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1); background-color: #fff; text-decoration: none; color: #333;">
  <div style="display: flex; align-items: center;">
    <div style="width: 50px; height: 50px; border-radius: 50%; border: 2px solid #e1e1e1; overflow: hidden; margin-right: 16px; display: flex; align-items: center; justify-content: center;">
      <img src="https://file.lifebus.top/imgs/gitea_logo.png" alt="Gitea" width="32px" style="width: 32px; object-fit: contain;">
    </div>
    <div style="flex: 1;">
      <div style="font-weight: bold;">Gitea</div>
      <div style="color: #555; font-size: 0.9em;">国内镜像发布仓库</div>
    </div>
  </div>
</a>

<a href="https://www.youtube.com/@cn_ms_studio" target="_blank" style="display: block; width: 300px; margin: 16px auto; padding: 16px; border: 1px solid #e1e1e1; border-radius: 8px; box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1); background-color: #fff; text-decoration: none; color: #333;">
  <div style="display: flex; align-items: center;">
    <div style="width: 50px; height: 50px; border-radius: 50%; border: 2px solid #e1e1e1; overflow: hidden; margin-right: 16px; display: flex; align-items: center; justify-content: center;">
      <img src="https://file.lifebus.top/imgs/google_youtube_logo.png" alt="YouTube" width="32px" style="width: 32px; object-fit: contain;">
    </div>
    <div style="flex: 1;">
      <div style="font-weight: bold;">YouTube</div>
      <div style="color: #555; font-size: 0.9em;">Google旗下的视频分享网站</div>
    </div>
  </div>
</a>

<a href="https://twitter.com/ms2297248353" target="_blank" style="display: block; width: 300px; margin: 16px auto; padding: 16px; border: 1px solid #e1e1e1; border-radius: 8px; box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1); background-color: #fff; text-decoration: none; color: #333;">
  <div style="display: flex; align-items: center;">
    <div style="width: 50px; height: 50px; border-radius: 50%; border: 2px solid #e1e1e1; overflow: hidden; margin-right: 16px; display: flex; align-items: center; justify-content: center;">
      <img src="https://file.lifebus.top/imgs/twitter_x_logo.png" alt="Twitter" width="32px" style="width: 32px; object-fit: contain;">
    </div>
    <div style="flex: 1;">
      <div style="font-weight: bold;">Twitter</div>
      <div style="color: #555; font-size: 0.9em;">X. 正在发生的事</div>
    </div>
  </div>
</a>
